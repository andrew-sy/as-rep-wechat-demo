import React, { Component } from "react";
import {
  Switch,
  Route,
  NavLink,
  Redirect
} from "react-router-dom";

import Suggestion from "features/suggestion/Suggestion";
import Insights from "features/insights/Insights";

import "./App.scss";

class App extends Component {
  render() {
    return (
      <section className="app">
        <section className="body">
          <Switch>
            <Route path = "/suggestion">
              <Suggestion/>
            </Route>
            <Route path = "/insights">
              <Insights/>
            </Route>
            <Redirect from="/" to="/suggestion"></Redirect>
          </Switch>
        </section>
        <div className="main-menu">
          <NavLink to="/suggestion">
            <span className="icon"></span>
            <span className="title">Suggestion</span>
          </NavLink>
          <span className="spliter"></span>
          <NavLink to="/insights">
            <span className="icon"></span>
            <span className="title">Insight</span>
          </NavLink>
        </div>
      </section>
    );
  }
}

export default App;
