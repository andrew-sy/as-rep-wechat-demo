import React from "react";

import "./DomainInsights.scss";

const DomainInsights = () => {
  return (
    <section className="domain-insights">
      <div className="safari-style">
        <section className="fragment">
          <div className="fragment-title">
            <span></span>
            <span>Sales Result Overview</span>
          </div>
          <div className="fragment-content xs">
            <div className="xs-item">
              <span>70%</span>
              <span>MTD Sales Achievement</span>
            </div>
            <hr />
            <div className="xs-item">
              <span>60%</span>
              <span>QTD Sales Growth</span>
            </div>
            <hr />
            <div className="xs-item">
              <span className="rank">
                <span>No.</span>
                <span>12</span>
              </span>
              <span>QTD Sales Volume Ranking</span>
            </div>
          </div>
        </section>
        <section className="fragment">
          <div className="fragment-title">
            <span></span>
            <span>Hospital Listing Overview</span>
          </div>
          <div className="fragment-content jy">
            <div className="item jy-item special">
              <span>Listed Hospitals</span>
              <div>
                <span>15</span>
                <span>+1 since last week</span>
              </div>
            </div>
            <hr />
            <div className="item jy-item">
              <span>Formal Listing</span>
              <div>
                <span>10</span>
                <span>></span>
              </div>
            </div>
            <hr />
            <div className="item jy-item">
              <span>Temp Purchased</span>
              <div>
                <span>5</span>
                <span>></span>
              </div>
            </div>
          </div>
        </section>
        <section className="fragment">
          <div className="fragment-title">
            <span></span>
            <span>Customer Engagement Overview</span>
          </div>
          <div className="fragment-content zx1">
            <div className="item zx1-item">
              <span>Number of targeted HCPs</span>
              <div>
                <span>400</span>
                <span>same since last week</span>
              </div>
            </div>
            <hr />
            <div className="item zx1-item">
              <span>Number of rep-focused HCPs</span>
              <div>
                <span>50</span>
                <span>same since last week</span>
              </div>
            </div>
          </div>
          <div className="fragment-content zx2">
            <div className="item zx2-item">
              <span>Number of meeting(MTD)</span>
              <div>
                <span>SM:6</span>
                <span>+1 since last week</span>
              </div>
              <div>
                <span>NNMM:1</span>
                <span>same since last week</span>
              </div>
            </div>
            <hr />
            <div className="item zx2-item">
              <span>Number of meeting covering rep-focused HCPs(MTD)</span>
              <div>
                <span>SM:6</span>
                <span>+1 since last week</span>
              </div>
              <div>
                <span>NNMM:1</span>
                <span>same since last week</span>
              </div>
            </div>
            <hr />
            <div className="item zx2-item">
              <span>Number of speaker invitation(MTD)</span>
              <div>
                <span>SM:5</span>
                <span>+1 since last week</span>
              </div>
              <div>
                <span>NNMM:1</span>
                <span>same since last week</span>
              </div>
            </div>
            <hr />
            <div className="item zx2-item">
              <span>Number of speaker invitation on rep-focused HCPs(MTD)</span>
              <div>
                <span>SM:3</span>
                <span>+1 since last week</span>
              </div>
              <div>
                <span>NNMM:1</span>
                <span>same since last week</span>
              </div>
            </div>
          </div>
        </section>
        <section className="fragment">
          <div className="fragment-title">
            <span></span>
            <span>Engagement rate of Actalya</span>
          </div>
          <div className="fragment-content xn">
            <div className="item xn-item">
              <span>Rep engagement rate</span>
              <div>
                <span>90%</span>
                <span></span>
              </div>
            </div>
            <hr />
            <div className="item xn-item">
              <span>Suggestion acceptance rate</span>
              <div>
                <span>80%</span>
                <span></span>
              </div>
            </div>
          </div>
        </section>
      </div>
    </section>
  );
};

export default DomainInsights;
