import React, { Component } from "react";

import "./Suggestion.scss";

import SuggestionItem from "components/suggestion-item/SuggestionItem";
import * as ExpandCollapse from "../../helpers/expand-collapse";

class Suggestion extends Component {
  render() {
    return (
      <section className="suggestion">
        <header>DM Suggestion</header>
        <div className="content">
          <div className="cards">
            <div>
              <SuggestionItem repName="Rep1" decrePct="12%" suggestion="Sales achievement rate(MTD) was below team average:  60% vs. 65%"></SuggestionItem>
              <SuggestionItem repName="Rep2" decrePct="12%" suggestion="Sales achievement rate(MTD) was below national average: 65% vs. 68%"></SuggestionItem>
              <SuggestionItem repName="Rep3" decrePct="3%" suggestion="Sales Volume QoQ(accumulated by month average) was below national average: 5% vs. 6%"></SuggestionItem>
              <SuggestionItem repName="Rep4" decrePct="22%" suggestion="Sales Volume QoQ(accumulated by month average) declined 22% "></SuggestionItem>
              <SuggestionItem repName="Rep5" decrePct="16%" suggestion="Sales Volume QoQ(accumulated by month average) declined 16% "></SuggestionItem>
              <SuggestionItem repName="Rep6" decrePct="5%" suggestion="Sales Volume QoQ(accumulated by month average) declined 5% "></SuggestionItem>
            </div>
          </div>
        </div>
      </section>
    );
  }

  componentDidMount() {
    ExpandCollapse.init();
  }
}

export default Suggestion;
