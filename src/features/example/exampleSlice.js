import {createSlice} from "@reduxjs/toolkit";
import ApiService from "../../api/ApiService";

// slicer
export const slice = createSlice({
    name: "example",
    initialState: {
        result: "Result in here"
    },
    reducers: {
        updateExample: (state, action) => {
            state.result = action.payload;
        }
    }
});

// action
export const { updateExample } = slice.actions;

// async request
export const requestExample = (id) => async (dispatch) => {
    const data = await ApiService.fetchExample(id);
    dispatch(updateExample(data.content));
}

// select state func
export const selectResult = (state) => state.example.result;

// reducer
export default slice.reducer;