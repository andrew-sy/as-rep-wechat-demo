import React, { useState } from "react";
import {useSelector, useDispatch} from "react-redux";
import { Grid, Header, Input, Button } from 'semantic-ui-react'
import {
    requestExample,
    selectResult
} from "./exampleSlice";
import "./Example.scss";
import TextDisplay from "../../components/textDisplay/TextDisplay";

const Example = () => {
    const result = useSelector(selectResult);
    const dispatch = useDispatch();
    const [id, setId] = useState("1");

    return(
        <Grid centered>
            <Grid.Row>
                <Header as='h1'>Example</Header>
            </Grid.Row>
            <Grid.Row>
                <Input icon='search' iconPosition='left' placeholder="Search..." value={id} onChange={(e) => setId(e.target.value)}/>
                <Button primary onClick={() => dispatch(requestExample(id))}>Search</Button>
            </Grid.Row>
            <Grid.Row>
                <TextDisplay text={result} />
            </Grid.Row>
        </Grid>
    )
}

export default Example;
