import React, {Component} from 'react';

import "./DoctorInsights.scss";
import DoctorItem from 'components/doctor-item/DoctorItem';
import * as ExpandCollapse from "../../helpers/expand-collapse";

class DoctorInsights extends Component {
    render () {
        return (
            <section className="doctor-insights">
                <div className="fragment-item">
                    <div className="header">
                        <span className="picture"></span>
                        <div className="brief">
                            <div className="brief-left">
                                <span>Huashan Hospital</span>
                                <span>MTD Sales Achievement: 60%</span>
                                <span>Entresto Market Share: 15%</span>
                            </div>
                            <div className="brief-right">
                                <span></span>
                                <span>Rep1</span>
                            </div>
                        </div>
                    </div>
                    <section className="content">
                        <DoctorItem keshi="Cardiology" headBgr="light-blue"/>
                        <DoctorItem keshi="Nephrology" headBgr="green-yellow"/>
                    </section>
                </div>  
            </section>
        );
    }
    
    componentDidMount() {
        ExpandCollapse.init();
    }

};

export default DoctorInsights;