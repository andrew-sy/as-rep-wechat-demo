import React, { Component } from "react";

import "./HospitalInsights.scss";

import HospitalItem from "../../components/hospital-item/HospitalItem";
import * as ExpandCollapse from "../../helpers/expand-collapse";

class HospitalInsights extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [
        {
          hospitalName: "Huasan Hospital",
          salesRate: "60%",
          marketPct: "15%",
          repName: "Rep1",
        },
        {
          hospitalName: "Jinan Central Hospital",
          salesRate: "50%",
          marketPct: "12%",
          repName: "Rep2",
        },
        {
          hospitalName: "Changhai Hospital",
          salesRate: "63%",
          marketPct: "17%",
          repName: "Rep2",
        },
        {
          hospitalName: "Tongji Hospital",
          salesRate: "54%",
          marketPct: "14%",
          repName: "Rep3",
        },
        {
          hospitalName: "Tongren Hospital",
          salesRate: "38%",
          marketPct: "11%",
          repName: "Rep3",
        },
        {
          hospitalName: "No.9 Hospital",
          salesRate: "58%",
          marketPct: "14%",
          repName: "Rep3",
        },
      ],
      rep: "",
      visibleRepSelection: false,
    };
  }
  render() {
    return (
      <section className="hospital-insights">
        <div>
        <section className="fragment">
          <div className="fragment-title">
            <div className="title">
              <span></span>
              <span>HCO list</span>
            </div>
            <div className="rep-select" onClick={() => this.showRepSelection()}>
              <span>Choose Rep</span>
            </div>
          </div>
          <div className="fragment-content">
            {this.state.data
              .filter((hp) => hp.repName.indexOf(this.state.rep) > -1)
              .map((hosp) => {
                return (
                  <HospitalItem
                    key={hosp.hospitalName}
                    hospitalName={hosp.hospitalName}
                    salesRate={hosp.salesRate}
                    marketPct={hosp.marketPct}
                    repName={hosp.repName}
                  />
                );
              })}
          </div>
        </section>
        <section
          className={`selection ${
            this.state.visibleRepSelection ? "visible" : ""
          }`}
        >
          <span className="title">Choose Rep</span>
          <ul>
            {Array.from(
              new Set(this.state.data.map((hosp) => hosp.repName))
            ).map((rep) => {
              return (
                <li
                  className={this.state.rep === rep ? "active" : ""}
                  key={rep}
                  onClick={() => this.selectRep(rep)}
                >
                  {rep}
                </li>
              );
            })}
          </ul>
        </section>
        </div>
      </section>
    );
  }
  componentDidMount() {
    ExpandCollapse.init();
  }

  showRepSelection() {
    this.setState({ visibleRepSelection: true });
  }

  selectRep(rep) {
    this.setState({
      rep: rep,
    });

    setTimeout(() => {
      this.setState({
        visibleRepSelection: false,
      });
    }, 300);
  }
}

export default HospitalInsights;
