import React from 'react';
import {
    Switch,
    Route,
    NavLink,
    Redirect
  } from "react-router-dom";

import "./Insights.scss";
import DomainInsights from 'features/domain-insights/DomainInsights';
import HospitalInsights from 'features/hospital-insights/HospitalInsights';
import DoctorInsights from 'features/doctor-insights/DoctorInsights';

const Insights = () => {
    return (
        <section className="insights">
            <header>
                <span className="title">DM Insight</span>
                <nav>
                    <NavLink to="/insights/domain">
                        <span>Territory Insight</span>
                        <div></div>
                    </NavLink>
                    <NavLink to="/insights/hospital">
                        <span>HCO Insight</span>
                        <div></div>
                    </NavLink>
                    <NavLink to="/insights/doctor">
                        <span>HCP Insight</span>
                        <div></div>
                    </NavLink>
                </nav>
            </header>
            <div className="content">
            <Switch>
                <Route path = "/insights/domain">
                    <DomainInsights/>
                </Route>
                <Route path = "/insights/hospital">
                    <HospitalInsights/>
                </Route>
                <Route path = "/insights/doctor">
                    <DoctorInsights/>
                </Route>
                <Redirect from="/" to="/insights/domain"></Redirect>
            </Switch>
            </div>
        </section>
    )
};

export default Insights;