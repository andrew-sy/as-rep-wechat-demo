import $ from 'jquery';

export const init = function () {
    $(".ec").find(".ec-trigger").click(function() {
        let $ecPanel = $(this).parents(".ec").first().find('.ec-panel').first();
        let expData = $(this).data('expand')
        let colData = $(this).data('collapse');

        let expIcon = $(this).data('expandIcon');
        let colIcon = $(this).data('collapseIcon');

        if ($(this).hasClass("expand")) {
            $ecPanel.css('height', 0);
            if (expData) {
                $(this).text(expData);
            }
            if (expIcon) {
                $(this).toggleClass(expIcon);
                $(this).toggleClass(colIcon);
            }
        } else {
            $ecPanel.css('height', 'auto');
            if (colData) {
                $(this).text(colData);
            }
            if (colIcon) {
                $(this).toggleClass(expIcon);
                $(this).toggleClass(colIcon);
            }
        }
        
        $(this).toggleClass("expand");
    })
};