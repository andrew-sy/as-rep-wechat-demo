import React, { Component } from "react";
import "./DoctorItem.scss";
import * as ExpandCollapse from "../../helpers/expand-collapse";

class DoctorItem extends Component {
  render() {
    return (
      <div className="doctor-item ec">
          <div className={`item-header ${this.props.headBgr}`}>
              <span>{this.props.keshi} Department</span>
              <span className="exp-col-btn ec-trigger expand-icon expand" data-expand-icon="expand-icon" data-collapse-icon="collapse-icon"></span>
          </div>
          <div className="item-content ec-panel">
              <div className="overview">
                <span className="overview-title">Department insight</span>
                <div className="overview-item">
                  <div className="left">Current month SM speaker stats (3 times):</div>
                  <div className="right">
                    <span><b>Focused HCP(1 times):</b> Dr.Wang(2 times)</span>
                    <span><b>Non focused HCP(1 times):</b> Dr.Li(1 time)</span>
                  </div>
                </div>
                <div className="overview-item">
                  <div className="left">Current month NNMM speaker stats (1 time):</div>
                  <div className="right">
                    <span><b>Focused HCP(1 times):</b> Dr.Sun(1 times)</span>
                  </div>
                </div>
              </div>
              <div className="detail">
                <div className="detail-title">
                  <span></span>
                  <span>Dr.Wang</span>
                  <span>{this.props.keshi}</span>
                  <span>Chief Physician</span>
                </div>
                <div className="detail-content">
                  <div className="detail-item">
                    <span>SM meeting history in last 180 days</span>
                    <span>2019/12/21, Topic：*****, Product: ***，Content:*****, Role:Speaker, Inviter:*****</span>
                  </div>
                  <div className="detail-item">
                    <span>NNMM meeting history in last 180 days</span>
                    <span>2019/12/21, Topic：*****, Product: ***，Content:*****, Role:Attendee, Inviter:*****</span>
                  </div>
                </div>
              </div>
          </div>
      </div>
    );
  }

  componentDidMount() {
    ExpandCollapse.init();
  }
}

export default DoctorItem;
