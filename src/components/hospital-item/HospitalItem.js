import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import "./HospitalItem.scss";
import * as ExpandCollapse from "../../helpers/expand-collapse";

class HospitalItem extends Component {
  render() {
    return (
      <section className="hospital-item ec">
        <div className="header">
          <span className="picture"></span>
          <div className="brief">
            <div className="brief-left">
              <span>{this.props.hospitalName}</span>
              <span>MTD Sales Achievement:{this.props.salesRate}</span>
              <span>Entrsto Market Share:{this.props.marketPct}</span>
            </div>
            <div className="brief-right">
              <span
                className="exp-col-btn ec-trigger"
                data-expand="Expand"
                data-collapse="Collapse"
              >
                Expand
              </span>
              <span></span>
              <span>{this.props.repName}</span>
            </div>
          </div>
        </div>
        <div className="content ec-panel">
          <h1 className="content-title">HCO Insight</h1>
          <div className="content-item">
            <span className="item-title">Sales achievement</span>
            <div className="item-content">
              <div>
                <span>Current month sales target: 1000</span>
                <span>achievement: 51%</span>
              </div>
              <div>
                <span>QoQ sales growth: 7%</span>
                <span>QTD sales achievement: 105%</span>
              </div>
            </div>
          </div>
          <div className="content-item">
            <span className="item-title">Hospital listing</span>
            <div className="item-content">
              <div>
                <span>Current status: Formal listing</span>
              </div>
            </div>
          </div>
          <div className="content-item">
            <span className="item-title">Market campaign status</span>
            <div className="item-content">
              <div>
                <span>Heart failure center: Wait for verification </span>
              </div>
            </div>
          </div>
          <div className="content-item">
            <span className="item-title">Customer coverage</span>
            <div className="item-content">
              <div>
                <span>No. of target HCPs: 30</span>
                <span>(rep-Focused HCPs: 10)</span>
              </div>
            </div>
          </div>
          <div className="content-item">
            <span className="item-title">Resource allocation</span>
            <div className="item-content">
              <div>
                <span>Current month SM speaker stats (3 times):</span>
                <span>Focused HCPs(2 times)</span>
              </div>
              <div>
                <span></span>
                <span>Non-Focused HCPs(1 time)</span>
              </div>
              <div>
                <span>Current month NNMM speaker stats (3 times):</span>
                <span>Focused HCPs(1 time)</span>
              </div>
              <div>
                <span></span>
                <span>Non-Focused HCPs (2 times)</span>
              </div>
            </div>
          </div>
          <div className="search">
            <NavLink to="/insights/doctor">
              <span className="search-icon"></span>
              <span>Department and HCP Insight</span>
            </NavLink>
          </div>
        </div>
      </section>
    );
  }

  componentDidMount() {
    ExpandCollapse.init();
  }
}

export default HospitalItem;
