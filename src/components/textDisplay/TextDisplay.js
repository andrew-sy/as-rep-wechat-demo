import React, { Component } from "react";
import "./TextDisplay.scss";

class TextDisplay extends Component {
  render() {
    return <span className="text">{this.props.text}</span>;
  }
}

export default TextDisplay;
