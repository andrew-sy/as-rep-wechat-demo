import React, { Component } from "react";
import "./SuggestionItem.scss";

class SuggestionItem extends Component {
  handleClick(actionName, e) {
    console.log(e.clientX);
    console.log(e.clientY);

    if (actionName === "open") {
      document.getElementById("pop-panel").style.display = "block";
      document.getElementById("hide-back").style.display = "block";
    } else if (actionName === "close") {
      document.getElementById("pop-panel").style.display = "none";
      document.getElementById("hide-back").style.display = "none";
    } else {
      alert("action not correct!");
    }
  }

  render() {
    return (
      <section className="card-content ec">
        <div className="item">
          <div>
            <span className="icon"></span>
            <span>{this.props.repName}</span>
          </div>
          <span
            className="btn-spread ec-trigger"
            data-expand="Expand"
            data-collapse="Collapse"
          >
            Expand
          </span>
        </div>
        <div className="row-content">
          <p>{this.props.suggestion}</p>
        </div>
        <div className="panel1 ec-panel ec">
          <div className="dot-line" />

          <div className="rep-content">
            <div className="p1-header">Sales Performance</div>
            <li>
              Changhai Hospital's sales volume QoQ declined{" "}
              {this.props.decrePct}, please follow up
            </li>
            <div
              className="l-span"
              onClick={(e) => this.handleClick("open", e)}
            >
              <button className="icon-fdb"></button>
              <div>Feedback</div>
            </div>
          </div>

          <div className="rep-content">
            <div className="p1-header">Hospital Listing</div>
            <li>
            Jinan central hospital 3M-after-listing sales achievement was 500 boxes which was below the alert benchmark: 600 boxes (data from: listing predicted curve).
            </li>
            <div
              className="l-span"
              onClick={(e) => this.handleClick("open", e)}
            >
              <button className="icon-fdb"></button>
              <div>Feedback</div>
            </div>
          </div>

          <div className="rep-content">
            <div className="p1-header">Strategic Department Engagement Efficiency</div>
            <span className="sub-title">Lack of resourcing: </span>
            <li>
            Huashan hospital cardiology HCPs
            <span className="doctor-link ec-trigger"><u>(ST:1 T1:2)</u></span> did not attend any meeting in last 60 days
            </li>
            <span className="sub-title">Over resourcing: </span>
            <li>Huashan hospital cardiology HCPs(ST:1) attended 2 or more meeting(as speaker or attendee) in last 45 days
             </li>
            <div
              className="l-span"
              onClick={(e) => this.handleClick("open", e)}
            >
              <button className="icon-fdb"></button>
              <div>Feedback</div>
            </div>
          </div>

          <div className="panel2 ec-panel">
            <div className="p-header">HCP list</div>
            <div>
              <span className="icon-rep-blue"></span>
              <span>Dr. Zhang, Chief Physician, Seg: ST</span>
            </div>
            <div>Last joined meeting：*****</div>
            <div>
              <div className="left">Role: Speaker</div>
              <div className="right">Date: 2019/12/15</div>
            </div>
            <div className="aline" />
            <div>
              <span className="icon-rep-blue"></span>
              <span>Dr. Li, Deputy Chief Physician, Seg: T1</span>
            </div>
            <div>Last joined meeting：*****</div>
            <div>
              <div className="left">Role: Attendee</div>
              <div className="right">Date: 2019/11/26</div>
            </div>
            <div className="aline" />
            <div>
              <span className="icon-rep-blue"></span>
              <span>Dr. Wang, Deputy Chief Physician, Seg: T1</span>
            </div>
            <div>Last joined meeting：*****</div>
            <div>
              <div className="left">Role: Attendee</div>
              <div className="right">Date: 2020/03/16</div>
            </div>
          </div>
        </div>
        <div id="pop-panel" className="pop-panel">
          <div className="pop-content">
            <div className="close-bar">
              <button
                className="icon-close"
                onClick={(e) => this.handleClick("close", e)}
              ></button>
            </div>
            <div className="pop-header">
              <div className="icon-arrow-down"></div>
              Remind frequency:
            </div>
            <form>
              <label>
                <input name="1" type="radio" value="" />
                Do not remind in next week.
              </label>

              <label>
                <input name="1" type="radio" value="" />
                Do not remind in next two weeks.{" "}
              </label>

              <label>
                <input name="1" type="radio" value="" />
                Do not remind in next four weeks.{" "}
              </label>
            </form>
            <div className="pop-header">
              <div className="icon-arrow-down"></div>
              Do not accept due to:
            </div>
            <form>
              <label>
                <input name="2" type="radio" value="" />
                Not prioritized HCO.
              </label>
              <label>
                <input name="2" type="radio" value="" />
                Data is not accurate.{" "}
              </label>
              <label>
                <input name="2" type="radio" value="" />
                It is not time-sensitive.{" "}
              </label>
            </form>
            <div className="btn-submit">
              <button onClick={(e) => this.handleClick("close", e)}>
                Submit
              </button>
            </div>
          </div>
        </div>
        <div id="hide-back" className="pop-hide-panel"></div>
      </section>
    );
  }
}

export default SuggestionItem;
