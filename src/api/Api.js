export default {
    baseUrl: process.env.REACT_APP_API_ENDPOINT,
    example: {
        url: `/example`,
    }
}