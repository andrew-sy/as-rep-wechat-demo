import Api from "./Api.js";

const fetchExample = function(id) {
    const url = `${Api.baseUrl}${Api.example.url}?id=${id}`;
    return new Promise((resolve, reject) => {
        fetch(url)
        .then((response) => response.json())
        .then((data) => {
            resolve(data);
        })
        .catch((error) => {
            reject(`Error in request: ${error}`);
        })
    });
}

export default {
    fetchExample
}