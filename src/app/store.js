import { configureStore } from "@reduxjs/toolkit";
import exampleReducer from "../features/example/exampleSlice";

export default configureStore({
  reducer: {
    example: exampleReducer
  },
});
