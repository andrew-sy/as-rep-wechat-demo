#!/bin/bash

echo "Stoping as-rep-wechat-demo container"
docker stop as-rep-wechat-demo

echo "Remove as-rep-wechat-demo container"
docker rm as-rep-wechat-demo

echo "Login Jfrog docker registry"
docker login aktanarepo-as-docker-cicd-local.jfrog.io --username="deployer" --password="AP6TBvrz1QFeasN3UDZngZxX8yZ"

if [ $# -eq 0 ]; then
    echo "Pulling lastest as-rep-wechat-demo image"
    docker pull aktanarepo-as-docker-cicd-local.jfrog.io/as-rep-wechat-demo
    echo "Setup new as-rep-wechat-demo container base on latest image"
    docker run --name as-rep-wechat-demo -d -p 4002:80 aktanarepo-as-docker-cicd-local.jfrog.io/as-rep-wechat-demo
else
    echo "Pulling as-rep-wechat-demo image version $1"
    docker pull aktanarepo-as-docker-cicd-local.jfrog.io/as-rep-wechat-demo:$1
    echo "Setup new as-rep-wechat-demo container base on version $1 image"
    docker run --name as-rep-wechat-demo -d -p 4002:80 aktanarepo-as-docker-cicd-local.jfrog.io/as-rep-wechat-demo:$1
fi